import 'posts.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:html' as html;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '@dshevchenkoo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int indexActiveChannel = 0;

  final channels = [
    'general',
    'projects',
    'startups',
  ];

  buildChannelPanel() {
    return Container(
      padding: EdgeInsets.only(top: 30, left: 20, right: 20),
      color: Color(0xf202122).withAlpha(200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'CHANNELS',
                style: TextStyle(color: Color(0xff919191)),
              ),
              Text(channels.length.toString(),
                  style: TextStyle(color: Color(0xff919191))),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          ...channels.asMap().keys.map((e) => Padding(
                padding: const EdgeInsets.only(top: 1.0),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      indexActiveChannel = e;
                    });
                  },
                  child: Container(
                    decoration: e == indexActiveChannel
                        ? BoxDecoration(
                            color: Colors.white.withAlpha(20),
                            borderRadius: BorderRadius.circular(5),
                          )
                        : null,
                    padding: EdgeInsets.only(top: 7, bottom: 7, left: 10),
                    width: double.infinity,
                    child: Text('# ' + channels[e],
                        style: TextStyle(
                            color: e == indexActiveChannel
                                ? Colors.white
                                : Color(0xffB5B5B5),
                            fontWeight: e == indexActiveChannel
                                ? FontWeight.w600
                                : FontWeight.w500)),
                  ),
                ),
              )),
        ],
      ),
    );
  }

  buildSocialNetworkButton(icon, url, name, {color, gradient}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        onTap: () {
          html.window.open(url, name);
        },
        child: Container(
          width: 50.0,
          height: 50.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: gradient,
            color: color, // inner circle color
          ),
          // color: Colors.blue,
          child: SvgPicture.asset(icon,
              height: 20,
              width: 20,
              color: Colors.white,
              fit: BoxFit.scaleDown),
        ),
      ),
    );
  }

  buildSocialNetworkPanel() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      color: Colors.black.withAlpha(200),
      child: Column(
        children: [
          buildSocialNetworkButton(
              'icons/telegram.svg', 'https://t.me/dshevchenkoo', 'dshevchenkoo',
              color: Colors.blue),
          buildSocialNetworkButton('icons/instagram.svg',
              'https://www.instagram.com/dshevchenkoo', 'dshevchenkoo',
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.purple, Colors.orange]))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/main_background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Row(
          children: [
            Expanded(child: buildSocialNetworkPanel()),
            Expanded(flex: 4, child: buildChannelPanel()),
            Expanded(
                flex: 12,
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        Container(
                          height: 60,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15.0),
                            child: Row(
                              children: [
                                Text(
                                  '#' + channels[indexActiveChannel],
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                            child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                height: 45,
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5.0),
                                    child: Image.asset(
                                      'assets/avatar.png',
                                      fit: BoxFit.cover,
                                    ))),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text('Shevchenko Dima',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(posts.keys.toList()[0],
                                        style: TextStyle(color: Colors.grey)),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                    width: 700,
                                    child: Text(posts.values.toList()[0]))
                              ],
                            )
                          ],
                        ))
                      ],
                    ),
                  ),
                )),
            Expanded(
                flex: 3,
                child: Container(
                  color: Color(0xfA8E959F).withAlpha(200),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset('assets/avatar.png'),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text(
                              'Shevchenko Dima',
                              style: TextStyle(
                                  fontWeight: FontWeight.w800, fontSize: 15),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(100)),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text('Software Flutter Engineer'),
                      )
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
